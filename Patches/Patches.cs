﻿using HarmonyLib;
using BepInEx;
using BepInEx.Logging;
using HarmonyLib.Tools;
using UnityEngine;

namespace Muckemble
{
    [HarmonyPatch]
    public static class Patches
    {
        [HarmonyPatch(typeof(PlayerMovement))]
        class CommonPlayerMovement
        {
            [HarmonyPatch(nameof(PlayerMovement.Movement)), HarmonyPrefix]
            static bool Movement(PlayerMovement __instance, float x, float y)
            {
                HarmonyFileLog.Enabled = true;
                FileLog.Log($"[{__instance.GetRb().position.x}, {__instance.GetRb().position.y}, {__instance.GetRb().position.z}]");
                FileLog.FlushBuffer();
                return true;
            }
        }
    }
}
